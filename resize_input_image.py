import numpy as np
import cv2
import os
#import sys
# sys.path.append('./Robust-Lane-Detection/LaneDetectionCode')
# import config

def resize_images(path, path_output):
    """ resizing all .jpg images in a folder with "path" 
        to fit the size 128x256 pxs for training and testing the model
    """
    ims_names = [name for name in os.listdir(path) if name.endswith('.jpg')]
    ims_names.sort()
    print(ims_names)
    cnt_name = 0
    for name in ims_names:
        image = cv2.imread(path+name)
        print(image.shape)
        resized = resize_images_onthefly(image)
        print(resized.shape)
        cv2.imwrite(path_output+str(cnt_name)+'.jpg', resized)
        cnt_name += 1

def binarize_image(image):
    lable_bin = image.copy()
    #lable_bin[image>=1] = 255
    return lable_bin

def resize_images_onthefly(image, label = False):
    """ resize one image on the fly """
    dsize = (256,128)
    #dsize = (img_width, img_height)
    if label == True: 
        binarized = binarize_image(image)
        return cv2.resize(binarized, dsize, interpolation = cv2.INTER_NEAREST)
    else:
        return cv2.resize(image, dsize, interpolation = cv2.INTER_CUBIC)

if __name__ == "__main__":
    path = "./data_resizing/image/"
    path_output = './data/testset/image/'
    resize_images(path, path_output)
