""" 8/01/20 by N.
    Script for generating txt files named "train_index", "val_index" and "test_index"
    to input images from a ___TuSimple___ dataset to train NN for Lane Detection. 
"""

def sorted_aphanumeric(data):
    """Sort list of names alpanumerically"""
    convert = lambda text: int(text) if text.isdigit() else text.lower()
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(data, key=alphanum_key)

# def write_names(names, output_path, N = 5, train_name = 'train_index.txt', val_name = 'val_index.txt'):
#     """write names of files ...  index.txt file"""
#     N_val = int(len(names)/10)
#     N_train = len(names) - N_val
#     print('Number of train sequences is {}'.format(N_train))
#     print('Number of validation sequences is {}'.format(N_val))
#     with open(output_path + train_name, 'a+') as outfile:
#         for i in range(N_train-N):
#             string = image_path+names[i] + ' ' + image_path+names[i+1] + ' ' + image_path+names[i+2] + ' ' + image_path+names[i+3] + ' ' + image_path+names[i+4] + ' ' + image_path+'segmentation_maps/'+names[i+4] + '\n' 
#             outfile.write(string)
        
#     with open(output_path + val_name, 'a+') as outfile:
#         for i in range(N_train, len(names)-N):
#             string = image_path+names[i] + ' ' + image_path+names[i+1] + ' ' + image_path+names[i+2] + ' ' + image_path+names[i+3] + ' ' + image_path+names[i+4] + ' ' + image_path+'segmentation_maps/'+names[i+4] + '\n' 
#             outfile.write(string)
    
#     print('Write into file {}'.format(outfile.name))

def write_names(names, image_path, label_path, output_path, N = 5, train_name = 'train_index.txt', val_name = 'val_index.txt'):
    """write names of files ...  index.txt file"""
    # print(len(names), ' <- inside function')
    N_val = int(len(names)/10)
    # if enough files to split for train and validaion
    if N_val > 10: 
        N_train = len(names) - N_val
        print('Number of train sequences is {}'.format(N_train))
        print('Number of validation sequences is {}'.format(N_val))
        with open(output_path + train_name, 'a+') as outfile:
            for i in range(N_train-N):
                lable_name = label_path + names[i+4][:-4] + '.png'
                string = image_path+names[i] + ' ' + image_path+names[i+1] + ' ' + image_path+names[i+2] + ' ' + image_path+names[i+3] + ' ' + image_path+names[i+4] + ' ' + lable_name + '\n' 
                outfile.write(string)

        with open(output_path + val_name, 'a+') as outfile:
            for i in range(N_train, len(names)-N):
                lable_name = label_path + names[i+4][:-4] + '.png'
                string = image_path+names[i] + ' ' + image_path+names[i+1] + ' ' + image_path+names[i+2] + ' ' + image_path+names[i+3] + ' ' + image_path+names[i+4] + ' ' + lable_name + '\n' 
                outfile.write(string)
    else: 
        # write just train set 
        N_train = len(names)
        print('*** Write only train sequences with length {}'.format(N_train))
        with open(output_path + train_name, 'a+') as outfile:
            for i in range(N_train-N):
                lable_name = label_path + names[i+4][:-4] + '.png'
                string = image_path+names[i] + ' ' + image_path+names[i+1] + ' ' + image_path+names[i+2] + ' ' + image_path+names[i+3] + ' ' + image_path+names[i+4] + ' ' + lable_name + '\n' 
                outfile.write(string)
  
    print('Write into file {}'.format(outfile.name))

def full_names_in_folder(path):
    """Get list of full names of files in all folders and write them to a files *index.txt (*train and validation sets)"""
    folders = []
    for r, d, f in os.walk(path):
        for folder in d:
            folders.append(os.path.join(r, folder))

    folders = sorted_aphanumeric(folders)
    for fold in folders:
        ext = '.png'
        names = sorted_aphanumeric([f for f in os.listdir(fold) if f.endswith(ext)]) # OR os.path.isfile ?
        if len(names)>0:
            full_names = [os.path.join(fold, name) for name in names]
            print(' ')
            print(full_names[0])
            print(len(full_names))
            
            write_names(names, image_path, label_path, output_path)

# train/validation split; take 90% of each
def index_of_one_scene(pattern):
    """write names of files of one name pattern to the index.txt file"""
    names = sorted_aphanumeric([f for f in os.listdir(image_path) if re.search(pattern, f)])
    write_names(names, image_path, label_path, output_path)


if __name__ == "__main__":
    import numpy as np
    import os 
    import cv2 
    import re

    root_path = '/home/dzyga/My/Python/ITJim/Projects/LaneDetection/lane-detection-nn/'
    #root_path = os.getcwd()
    train_name = 'train_index.txt'
    val_name = 'val_index.txt'    
    N = 5 #number of images in a one sequence with one label 
        
    # # --- TUSimple 
    # image_path = root_path + 'TUSimple_elements/images_annot/' # this is the input path! 
    # output_path = root_path + '/Robust-Lane-Detection/LaneDetectionCode/data/'
    
    # --- CULane
    image_path = './lane-detection-nn/CULane/'
    label_path = image_path
    output_path = '/home/dzyga/My/Python/ITJim/Projects/LaneDetection/'
    
    full_names_in_folder(image_path)

    if not os.path.exists(output_path):
        os.makedirs(output_path)

# # TuSimple ------------------------------------------------------
# # 1. '0313-1_60_20'
# pattern1 = r'0313-1.*\.jpg$'
# index_of_one_scene(pattern1)

# # 2. '0313-1_60_20'
# pattern2 = r'0313-2.*\.jpg$' 
# index_of_one_scene(pattern2)

# # 3. 0530_1492626047222176976_0_20.jpg
# pattern3 = r'0530_14926.*\.jpg$'
# index_of_one_scene(pattern3)

# # 4. 0530_1492720183607312769_0_20.jpg
# pattern4 = r'0530_149272.*\.jpg$'
# index_of_one_scene(pattern4)

# # 5. 0531_1492626253262712112_20.jpg
# pattern5 = r'0531_14926.*\.jpg$'
# index_of_one_scene(pattern5)

# # 6. 0531_1492720362332694269_20.jpg
# pattern6 = r'0531_149272.*\.jpg$'
# index_of_one_scene(pattern6)

# # 7. 0531_1495487718661629177_20.jpg
# pattern7 = r'0531_149548.*\.jpg$'
# index_of_one_scene(pattern7)

# # 8. 0601_1494452381594376146_20.jpg
# pattern8 = r'0601_149445.*\.jpg$'
# index_of_one_scene(pattern8)

# # 9. 0601_1495058507652622962_20.jpg
# pattern9 = r'0601_149505.*\.jpg$'
# index_of_one_scene(pattern9)

# # 10. 0601_1495485005644829619_20.jpg
# pattern10 = r'0601_14954.*\.jpg$'
# index_of_one_scene(pattern10)


